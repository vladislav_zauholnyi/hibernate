package app.tests;

import app.config.HibernateTestConfiguration;
import app.dao.interfaces.ItemDAO;
import app.dao.interfaces.UserDAO;
import app.dto.ReceiptDTO;
import app.dto.UserDTO;
import app.entity.Item;
import app.entity.User;
import app.service.interfaces.UserService;
import net.bytebuddy.utility.RandomString;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;

import javax.persistence.NoResultException;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@SpringJUnitConfig(HibernateTestConfiguration.class)
public class CRUDTests {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private UserService userService;

    @Autowired
    private ItemDAO itemDAO;

    private static final String[] ITEM_NAMES = {"Apple", "Pizza", "Hamburger", "Sushi"};
    private Map<String, Item> items = Collections.emptyMap();

    @BeforeEach
    public void initializeItems() {
        List<Item> initialItems = itemDAO.getItems();
        if (initialItems.isEmpty()) {
            initialItems = createItems();
        }

        items = initialItems.stream().collect(Collectors.toMap(Item::getName, Function.identity()));
    }

    private List<Item> createItems() {
        return Stream
                .of(ITEM_NAMES)
                .map(name -> itemDAO.create(name))
                .collect(Collectors.toList());
    }

    @Test
    public void createUserTest() {
        UserDTO userDTO = createUserDto(RandomString.make(10));
        UserDTO createdUser = userService.create(userDTO);
        Assertions.assertEquals(userDTO.getReceipts().size(), createdUser.getReceipts().size());
    }

    @Test
    public void testingCache(){
        User user = new User();
        user.setName("Name");
        user.setEmail("email@gmail.com");
        user.setReceipts(new ArrayList<>());
        userDAO.save(user);
        User getResult = userDAO.get(user.getId());
        User getResult2 = userDAO.get(user.getId());
        User getResult3 = userDAO.get(user.getId());

        int size = CacheManager.ALL_CACHE_MANAGERS.get(0).getCache(User.class.getName()).getSize();

        Assertions.assertTrue(size > 0);
    }

    @Test
    public void getUserTest() {
        String string = RandomString.make(10);
        UserDTO userDTO = createUserDto(string);
        userService.create(userDTO);
        UserDTO getResult = userService.get(string);

        Assertions.assertEquals(userDTO.getReceipts().size(), getResult.getReceipts().size());
    }

    @Test
    public void updateUserTest() {
        String string = RandomString.make(10);
        UserDTO userDTO = createUserDto(string);
        UserDTO createdUser = userService.create(userDTO);

        ReceiptDTO firstReceipt = createdUser.getReceipts().get(0);
        firstReceipt.setDescription("New description");
        firstReceipt.setItemID(getItemId(2));

        createdUser.setName("new name");
        createdUser.getReceipts().remove(1);

        ReceiptDTO newReceipt = new ReceiptDTO();
        newReceipt.setDescription("Another desc");
        newReceipt.setItemID(getItemId(3));

        createdUser.getReceipts().add(newReceipt);

        userService.update(createdUser);
        UserDTO updated = userService.get(string);

        // ** ASSERTIONS **

        Assertions.assertEquals("new name", updated.getName());
        Assertions.assertEquals(2, updated.getReceipts().size());

        ReceiptDTO firstUpdatedReceipt = updated.getReceipts().get(0);
        Assertions.assertNotNull(firstReceipt.getId());
        Assertions.assertEquals(getItemId(2), firstUpdatedReceipt.getItemID());

        ReceiptDTO secondUpdatedReceipt = updated.getReceipts().get(1);
        Assertions.assertNotNull(secondUpdatedReceipt.getId());
        Assertions.assertEquals(getItemId(3), secondUpdatedReceipt.getItemID());
        Assertions.assertEquals("Another desc", secondUpdatedReceipt.getDescription());
    }

    @Test
    public void deleteUserTest() {
        String string = RandomString.make(10);
        UserDTO userDTO = createUserDto(string);
        UserDTO createResult = userService.create(userDTO);

        Assertions.assertNotNull(createResult);
        userService.delete(createResult.getId());

        Assertions.assertThrows(NoResultException.class, () -> userService.get(createResult.getString()));
    }

    private UserDTO createUserDto(String string) {
        UserDTO userDTO = new UserDTO();
        userDTO.setString(string);
        userDTO.setName("Vladislav");

        ReceiptDTO firstReceipt = new ReceiptDTO();
        firstReceipt.setDescription("First receipt");
        firstReceipt.setItemID(getItemId(0));
        userDTO.getReceipts().add(firstReceipt);

        ReceiptDTO secondReceipt = new ReceiptDTO();
        secondReceipt.setDescription("Second receipt");
        secondReceipt.setItemID(getItemId(1));
        userDTO.getReceipts().add(secondReceipt);
        return userDTO;
    }

    private UUID getItemId(int itemIndex) {
        return items.get(ITEM_NAMES[itemIndex]).getId();
    }
}
