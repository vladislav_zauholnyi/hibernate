package app.service.impl;

import app.converter.interfaces.UserConverter;
import app.dao.interfaces.UserDAO;
import app.dto.UserDTO;
import app.entity.Receipt;
import app.entity.User;
import app.service.interfaces.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDAO userDAO;

    @Autowired
    private UserConverter userConverter;

    @Override
    public UserDTO create(UserDTO userDTO) {
        User user = userConverter.convertUser(userDTO);
        userDAO.save(user);
        return userConverter.convertUser(user);
    }

    @Override
    @Transactional(readOnly = true)
    public UserDTO get(String string) {
        User user = userDAO.get(string);
        return userConverter.convertUser(user);
    }

    @Override
    public UserDTO get(UUID id) {
        User user = userDAO.get(id);
        return userConverter.convertUser(user);
    }

    @Override
    @Transactional
    public UserDTO update(UserDTO userDTO) {
        User initialUser = userDAO.get(userDTO.getId());
        User updatedUser = userConverter.convertUser(userDTO);
        updateUser(initialUser, updatedUser);
        return userConverter.convertUser(initialUser);
    }

    private void updateUser(User initialUser, User updatedUser) {
        initialUser.setName(updatedUser.getName());
        initialUser.setEmail(updatedUser.getEmail());
        updateReceipts(initialUser.getReceipts(), updatedUser.getReceipts());
    }

    private void updateReceipts(List<Receipt> initialReceipts, List<Receipt> updatedReceipts) {
        Map<UUID, Receipt> stillExistentReceipts = updatedReceipts
                .stream()
                .filter(j -> Objects.nonNull(j.getId()))
                .collect(Collectors.toMap(Receipt::getId, Function.identity()));

        List<Receipt> receiptsToAdd = updatedReceipts
                .stream()
                .filter(j -> Objects.isNull(j.getId()))
                .collect(Collectors.toList());

        Iterator<Receipt> iterator = initialReceipts.iterator();
        while (iterator.hasNext()) {
            Receipt persistentReceipt = iterator.next();
            if (stillExistentReceipts.containsKey(persistentReceipt.getId())) {
                Receipt updatedReceipt = stillExistentReceipts.get(persistentReceipt.getId());
                updateReceipt(persistentReceipt, updatedReceipt);
            } else {
                iterator.remove();
                persistentReceipt.setUser(null);
            }
        }
        initialReceipts.addAll(receiptsToAdd);
    }

    private void updateReceipt(Receipt persistentReceipt, Receipt updatedReceipt) {
        persistentReceipt.setItem(updatedReceipt.getItem());
        persistentReceipt.setUser(updatedReceipt.getUser());
    }

    @Override
    @Transactional
    public void delete(UUID id) {
        userDAO.delete(id);
    }
}
