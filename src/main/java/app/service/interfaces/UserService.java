package app.service.interfaces;

import app.dto.UserDTO;

import java.util.UUID;

public interface UserService {
    UserDTO create(UserDTO userDTO);
    UserDTO get(String string);
    UserDTO get(UUID id);
    UserDTO update(UserDTO userDTO);
    void delete(UUID id);
}
