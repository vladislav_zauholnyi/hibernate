package app.converter.impl;

import app.converter.interfaces.UserConverter;
import app.dao.interfaces.ItemDAO;
import app.dto.ReceiptDTO;
import app.dto.UserDTO;
import app.entity.Item;
import app.entity.Receipt;
import app.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class UserConverterImpl implements UserConverter {

    @Autowired
    private ItemDAO itemDAO;

    @Override
    public User convertUser(UserDTO userDTO) {
        User user = new User();
        user.setId(userDTO.getId());
        user.setName(userDTO.getName());
        user.setEmail(userDTO.getString());

        Map<UUID, Item> receiptByID = getItems(userDTO);

        for (ReceiptDTO receiptDTO : userDTO.getReceipts()) {
            Receipt receipt = convertReceipts(receiptDTO);
            receipt.setUser(user);

            Item item = receiptByID.get(receiptDTO.getItemID());
            if (null == item) {
                throw new IllegalArgumentException("Item with ID: " + receiptDTO.getItemID() + " doesn't exist!");
            }

            receipt.setItem(item);
            user.getReceipts().add(receipt);
        }
        return user;
    }

    @Override
    public UserDTO convertUser(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setName(user.getName());
        userDTO.setString(user.getEmail());

        List<ReceiptDTO> receipts = user.getReceipts()
                .stream()
                .map(this::convertReceipts)
                .collect(Collectors.toList());

        userDTO.setReceipts(receipts);
        return userDTO;
    }

    private ReceiptDTO convertReceipts(Receipt receipt) {
        ReceiptDTO receiptDTO = new ReceiptDTO();
        receiptDTO.setId(receipt.getId());
        receiptDTO.setDescription(receipt.getDescription());
        receiptDTO.setItemID(receipt.getItem().getId());
        return receiptDTO;
    }

    private Map<UUID, Item> getItems(UserDTO userDTO) {
        List<UUID> receiptIDs = userDTO
                .getReceipts()
                .stream()
                .map(ReceiptDTO::getItemID)
                .collect(Collectors.toList());

        List<Item> items = itemDAO.getByIDs(receiptIDs);
        return items
                .stream()
                .collect(Collectors.toMap(Item::getId, Function.identity()));
    }

    private Receipt convertReceipts(ReceiptDTO receiptDTO) {
        Receipt receipt = new Receipt();
        receipt.setId(receiptDTO.getId());
        receipt.setDescription(receiptDTO.getDescription());
        return receipt;
    }
}
