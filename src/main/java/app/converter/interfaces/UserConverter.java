package app.converter.interfaces;

import app.dto.UserDTO;
import app.entity.User;

public interface UserConverter {
    User convertUser(UserDTO userDTO);

    UserDTO convertUser(User user);
}
