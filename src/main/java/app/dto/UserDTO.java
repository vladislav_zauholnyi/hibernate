package app.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@ToString
public class UserDTO {
    private UUID id;
    private String name;
    private String string;
    private List<ReceiptDTO> receipts = new ArrayList<>();
}
