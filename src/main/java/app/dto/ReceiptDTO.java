package app.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.UUID;

@Getter
@Setter
@ToString
public class ReceiptDTO {
    private UUID id;
    private String description;
    private UUID itemID;
}
