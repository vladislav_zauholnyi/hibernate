package app.dao.impl;

import app.dao.interfaces.ItemDAO;
import app.entity.Item;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.UUID;

@Repository
public class ItemDAOImpl implements ItemDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional(readOnly = true)
    public List<Item> getByIDs(List<UUID> itemIDs) {
        Session session = sessionFactory.getCurrentSession();
        return session.byMultipleIds(Item.class).multiLoad(itemIDs);
    }

    @Override
    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true)
    public List<Item> getItems() {
        return sessionFactory.getCurrentSession().createQuery("from Item").list();
    }

    @Override
    @Transactional
    public Item create(String name) {
        Item item = new Item();
        item.setName(name);
        sessionFactory.getCurrentSession().save(item);
        return item;
    }
}
