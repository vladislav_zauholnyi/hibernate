package app.dao.impl;

import app.dao.interfaces.UserDAO;
import app.entity.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.UUID;

@Repository
public class UserDAOImpl implements UserDAO {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    @Transactional
    public void save(User user) {
        Session session = sessionFactory.getCurrentSession();
        session.save(user);
    }

    @Transactional(readOnly = true)
    @Override
    public User get(String email) {
        Session session = sessionFactory.getCurrentSession();
        Object object = session
                .getNamedQuery("userByEmail")
                .setParameter("email", email)
                .getSingleResult();

        return (User) object;
    }

    @Transactional(readOnly = true)
    @Override
    public User get(UUID id) {
        Session session = sessionFactory.getCurrentSession();
        User user = session.get(User.class, id);
        return Objects.requireNonNull(user, "User with ID " + id + " doesn't exist");
    }

    @Override
    public void delete(UUID id) {
        User user = get(id);
        sessionFactory.getCurrentSession().delete(user);
    }
}
