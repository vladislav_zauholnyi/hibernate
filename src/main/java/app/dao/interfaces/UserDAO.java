package app.dao.interfaces;

import app.entity.User;

import java.util.UUID;

public interface UserDAO {
    void save(User user);

    User get(String email);

    User get(UUID id);

    void delete(UUID id);
}
