package app.dao.interfaces;

import app.entity.Item;

import java.util.List;
import java.util.UUID;

public interface ItemDAO {
    List<Item> getByIDs(List<UUID> itemIDs);

    List<Item> getItems();

    Item create(String name);
}
